import { render, screen, fireEvent } from '@testing-library/react';
import ScrabbleForm from './ScrabbleForm';
import { getPoints } from '../api/ScrabbleAPI'

const setup = () => {
  const utils = render(<ScrabbleForm
    id="points-checker"
    promise={getPoints}
    title="Point checker"
    label="Check for points"
    buttonText="Check"
    hasList={true} />);
  const input = screen.getByLabelText("Check for points");
  return {
    input,
    ...utils,
  }
}

test('allows input', () => {
  const { input } = setup();
  fireEvent.change(input, { target: { value: "asd" } });

  expect(input.value).toBe('asd')
});

test('allows only letters', () => {
  const {input} = setup();
  fireEvent.change(input, {target: {value: "12345"}});

  expect(input.value).toBe('')
});