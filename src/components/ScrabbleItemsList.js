import './ScrabbleItemsList.css';
import React from 'react';

class ScrabbleItemsList extends React.Component {
  render() {
    return (
      <ul>
        {this.props.items.map(item => (
          <li key={item.id}>{item.text} points - {item.value.points}, is in dictionary - {item.value.inDictionary.toString()}</li>
        ))}
      </ul>
    );
  }
}

export default ScrabbleItemsList;