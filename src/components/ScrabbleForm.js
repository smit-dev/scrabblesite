//Mikk Raudvere: this is based off of https://reactjs.org/ "An Application" tutorial 

import React, { Component } from 'react';
import ScrabbleItemsList from './ScrabbleItemsList';

class ScrabbleForm extends Component {
  constructor(props) {
    super(props);
    this.state = { items: [], text: '' };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  render () {
    return (
      <div>
        <h3>{this.props.title}</h3>
        <form onSubmit={this.handleSubmit}>
          <label htmlFor={this.props.id}>
            {this.props.label}
          </label>
          <input
            id={this.props.id}
            onChange={this.handleChange}
            value={this.state.text}
          />
          <button>
            {this.props.buttonText}
          </button>
        </form>
        {this.renderItemsList()}
      </div>);
  }

  renderItemsList () {
    if (this.props.hasList) {
      return <ScrabbleItemsList items={this.state.items} />;
    } else {
      return null;
    }
  }

  addNewItem (data) {
    const newItem = {
      text: this.state.text,
      id: Date.now(),
      value: data
    };
    this.setState(state => ({
      items: state.items.concat(newItem),
      text: ''
    }));
  }

  handleChange (e) {
    this.setState({ text: e.target.value.replace(/[^a-z]/gi, '')});
  }

  handleSubmit (e) {
    e.preventDefault();
    if (this.state.text.length === 0) {
      return;
    }
    this.props.promise(this.state.text)
      .then(data => {
        this.addNewItem(data);
      })
      .catch(error => {
        this.setState({ text: ''});
      });

  }
}
export default ScrabbleForm;