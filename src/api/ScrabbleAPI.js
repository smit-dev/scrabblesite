import axios from "axios";

const getPoints = (word) => {
  return axios.get(process.env.REACT_APP_SCRABBLE_API + '/get-points/' + word)
    .then(response => response.data);
}

const isWordInDictionary = (word) => {
  return axios.get(process.env.REACT_APP_SCRABBLE_API + '/contains-word/' + word)
    .then(response => response.data);
}

const addWord = (word) => {
  return axios.post(process.env.REACT_APP_SCRABBLE_API + '/words', {
    word: word
  })
    .then(response => response.data);
}

export { getPoints, isWordInDictionary, addWord };