import './App.css';
import ScrabbleForm from '../components/ScrabbleForm';
import { getPoints, addWord } from '../api/ScrabbleAPI'

function App () {
  return (
    <div className="App">
      <div className="App-body">
        <ScrabbleForm
          id="word-adder"
          promise={addWord}
          title="Word adder"
          label="Add word to dictionary"
          buttonText="Add word"
          hasList={false}>
        </ScrabbleForm><ScrabbleForm
          id="points-checker"
          promise={getPoints}
          title="Point checker"
          label="Check for points"
          buttonText="Check"
          hasList={true}
          >
        </ScrabbleForm>
      </div>
    </div>
  );
}

export default App;
