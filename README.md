# ScrabbleSite
Scrabble site helps to calculate how many point a word gives you and if the word is in the dictionary.

It depends on [ScrabbleAPI project](https://gitlab.com/smit-dev/scrabbleapi) to query points and add words to dictionary.

# Getting started
Node version 16 is needed for this project (`nvm use 16`).

To get frontend running locally:
* clone this repo
* `npm install` to install dependencies
* `npm start` to start local server

By default port 4040 is used.

## Environment variables
Configuration is in the .env file.

Port can be set with:
`PORT=4040`

Backend can be set with:
`REACT_APP_SCRABBLE_API=http://localhost:8080/api/scrabble`

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:4040](http://localhost:4040) to view it in your browser.


### `npm test`
Runs tests.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.
